# Local Directional Number Pattern

## Description

Computes LDN response, according to eight masks and returns the top directions
Direction for each pixel:
- pos (first) edge direction
- neg (second) gradient direction

## Usage

To code a given image you can use this code by

```matlab
% load image in I

% default invocation (mask Kirsch, and neighborhood size of 3x3)
C = ldn(I);

% using mask Sobel, and neighborhood size of 5x5)
C = ldn(I,'mask', 'sobel', 'masksize', 5);
```

## Citation

If you use this code please cite

> Ramirez Rivera, A.; Rojas Castillo; Oksam Chae, "Local Directional Number Pattern for Face Analysis: Face and Expression Recognition," Image Processing, IEEE Transactions on , vol.22, no.5, pp.1740,1752, May 2013 doi: 10.1109/TIP.2012.2235848

BibTeX:
```tex
@ARTICLE{Ramirez2013,
  author={Ram\'irez Rivera, A. and Rojas Castillo and Oksam Chae},
  journal={Image Processing, IEEE Transactions on},
  title={Local Directional Number Pattern for Face Analysis: Face and Expression Recognition},
  year={2013},
  month={May},
  volume={22},
  number={5},
  pages={1740--1752},
  doi={10.1109/TIP.2012.2235848},
  ISSN={1057-7149},
}
```